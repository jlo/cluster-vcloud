class zip {
   package { tar: 
      ensure  => latest,
   }
   package { zip: 
      ensure  => latest,
   }
   package { unzip: 
		ensure  => latest,
	}
	package { bzip2:
		ensure  => latest,
	}
	package { p7zip:
		ensure  => latest,
	}
	package { unrar:
		ensure  => latest,
	}
}

