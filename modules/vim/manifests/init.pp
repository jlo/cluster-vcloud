class vim {
	package { vim: 
		ensure  => latest,
	}
   file { "/etc/vim/vimrc":
		ensure  => file,
   	owner   => root,
   	group   => root,
   	mode    => 644,
   	source  => "puppet:///modules/vim/vimrc",
		require => Package[vim]
   }
}

