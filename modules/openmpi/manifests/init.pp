class openmpi {
     package {'openmpi-bin': ensure => installed}
     package {'mpi-default-dev': ensure => installed}
    }
