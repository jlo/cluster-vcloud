class groups {
   Class[nfs] -> Class[groups]
   group {'profs':
      ensure => present,
      gid => 2001,
   }
   group {'etudiants':
      ensure => present,
      gid => 2002,
   }
}