class apt {
   file { "/etc/apt/sources.list.d/unstable.list":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 644,
      source  => "puppet:///modules/apt/unstable.list"
   }
   file { "/etc/apt/apt.conf":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 644,
      content => "APT::Default-Release \"jessie\";\n",
   }
   exec { "/usr/bin/apt-get update ; /bin/true":
      alias       => aptgetupdate,
      subscribe   => [
         File["/etc/apt/sources.list.d/unstable.list"],
         File["/etc/apt/apt.conf"],
      ],
      refreshonly => true
   }
}

define unstable($paquets) {
   file { "/etc/apt/preferences.d/${name}":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 644,
      content => template("apt/unstable.preferences"),
   }
}
