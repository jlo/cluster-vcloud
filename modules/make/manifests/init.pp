class make {
	package { 'build-essential':
		ensure  => latest,
	}
   package { 'autoconf':
      ensure  => latest,
   }
	package { 'automake':
      ensure  => latest,
      require => Package['autoconf'],
   }
   package { 'libtool':
      ensure  => latest,
   }
   package { 'flex':
      ensure  => latest,
   }
   package { 'bison':
      ensure  => latest,
   }
}
