class slurm {
 ## SLURM setup
    package {'slurm-llnl': ensure => installed}
    package {'munge': ensure => installed}
    # slurm client
    service {"slurmd":
        ensure => 'running',
        enable => 'true',
        require => Package['slurm-llnl'],
    }
    if "${::hostname}" == "master" {
      service {"slurmctld":
        ensure => 'running',
        enable => 'true',
        require => Package['slurm-llnl'],
      }
    }

    ## Munge (SLURM authentication)
    service {"munge":
        ensure => 'running',
        enable => 'true',
        require => Package['munge'],
    }
    file{'/etc/slurm-llnl/slurm.conf':
        source => "puppet:///modules/slurm/slurm.conf",
        owner => "root",
        group => "root",
        require => Package['slurm-llnl'],
        notify => Service['slurmd'],
    }
    file { "/var/spool/slurmd/":
         ensure => 'directory',
         owner   => slurm,
         group   => slurm,
         mode    => 744,
         require => Package['slurm-llnl'],
      }
    file{'/etc/munge/munge.key':
        source => "puppet:///modules/slurm/munge.key",
        owner => "munge",
        group => "munge",
        mode => "400",
        notify => Service['munge'],
        require => Package['munge'],
    }
}
