class nfs {
	package { rpcbind: 
		ensure  => latest,
	}
   package { nfs-common:       
      ensure  => latest,
      require => Package[rpcbind]
   }
	if "${::hostname}" == "master" {
      package { nfs-kernel-server:       
         ensure  => latest,
         require => [ 
            Package[nfs-common], 
            Package[rpcbind]
         ]
      }
      file { "/etc/exports":
         ensure  => file,
         owner   => root,
         group   => root,
         mode    => 644,
         source  => "puppet:///modules/nfs/exports",
         require => Package["nfs-kernel-server"]
      }
      service { 'nfs-kernel-server':
         enable  => true,
         ensure  => running,
         require => [
            Package["nfs-kernel-server"],
            File["/etc/exports"],
         ],
      }
      file { "/usr/local/bin/keep-nfs-active":
         ensure  => file,
         owner   => root,
         group   => root,
         mode    => 700,
         content => "#!/bin/sh\n\nexport PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'\n\nps aux | grep nfsd | grep -v grep || service nfs-kernel-server restart\n\n",
         require => Service['nfs-kernel-server'],
      }
   }
   else {
      mount { "/home":
         device  => "master:/home",
         fstype  => "nfs",
         ensure  => present,
         options => "auto",
         atboot  => false,
		}
		file { "/usr/local/bin/keep-nfs-active":
         ensure  => file,
         owner   => root,
         group   => root,
         mode    => 700,
         content => "#!/bin/sh\n\nexport PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'\n\nmount -a\n\n",
         require => Mount['/home'],
      }
	}
	cron { "mount_nfs":
      ensure  => present,
      command => "/usr/local/bin/keep-nfs-active > /dev/null 2>&1",
      user    => 'root',
      require => File["/usr/local/bin/keep-nfs-active"],
   }
}
