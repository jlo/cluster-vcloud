class users {
   Class[groups] -> Class[users]
   User {
      ensure     => present,
      shell      => '/bin/bash',
      membership => inclusive,
      managehome => false,
   }
   compte { [ jlo ]:
      groups => [ profs, etudiants ]
   }
   compte { [ user01, user02, user03, user04, user05, user06, user07, user08 ]:
      groups => [ etudiants ]
   }
   if "${::hostname}" == "master" {
      file { "/usr/local/bin/create-compte":
         ensure  => file,
         owner   => root,
         group   => root,
         mode    => 700,
         source  => "puppet:///modules/users/create-compte",
      }
   }
   else {
      file { "/usr/local/bin/update-uid":
         ensure  => file,
         owner   => root,
         group   => root,
         mode    => 700,
         source  => "puppet:///modules/users/update-uid",
      }
      cron { "update_uid":
         ensure  => present,
         command => "/usr/local/bin/update-uid > /dev/null 2>&1",
         user    => 'root',
         require => File["/usr/local/bin/update-uid"],
      }
   }
}

define compte($groups) {
   user { $name:
      groups   => $groups
   }
   if "${::hostname}" == "master" {
      exec { "create-compte-$name":
         command     => "/usr/local/bin/create-compte ${name}",
         creates     => "/home/${name}/.ssh/id_rsa",
         path        => "/bin:/usr/bin:/usr/sbin:/sbin",
         cwd         => "/home",
         timeout     => 0,
         require     => [
            User["${name}"],
            File["/usr/local/bin/create-compte"],
         ],
      }
   }
}
