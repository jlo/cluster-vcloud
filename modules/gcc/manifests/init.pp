class gcc {
	package {"gcc": ensure => installed}
}



# class gcc {
#    unstable { "gcc": 
#       paquets => [ 
#          gcc-6, cpp-6, libcc1-0, binutils, libgcc-6-dev, 'libstdc++6', libmpfr4, libgcc1, libgomp1, libgcc-6-dev,
#          libgomp1, libitm1, libatomic1, liblsan0, libtsan0, libubsan0, libcilkrts5, libquadmath0
#       ]
#    }
# 	package { "gcc-6": 
# 		ensure  => latest,
#     require => Unstable["gcc"],
#    }
#    package { "g++-6": 
# 		ensure  => latest,
#     require => Package["gcc-6"],
#    }
#    file { "/usr/bin/gcc":
#       ensure  => symlink,
#       target  => "/usr/bin/gcc-6",
#       require => Package["gcc-6"],
#    }
#    file { "/usr/bin/g++":
#       ensure  => symlink,
#       target  => "/usr/bin/g++-6",
#       require => Package["g++-6"],
#    }
# }
# 
