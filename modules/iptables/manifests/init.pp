class iptables {
   Class[ssh] -> Class[iptables]
   case "${::hostname}" {
      'master': { $iptables = "puppet:///modules/iptables/iptables.master" }
      default: { $iptables = "puppet:///modules/iptables/iptables.node" }
   }
   file { '/etc/init.d/iptables':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 700,
      source  => $iptables,
   }
   service { iptables:
      enable  => true,
      require => File['/etc/init.d/iptables'],
   }
}
