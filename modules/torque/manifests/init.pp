class torque {
   package { torque-common:
      ensure  => latest
   }
   package { torque-client:
      ensure  => latest
   }
   if "${::hostname}" == "master" {
      exec { "server_priv_nodes":
         command  => "mkdir -p /var/spool/torque/server_priv ; cat /etc/hosts | awk '{ print \$2; }' | grep ^node > /var/spool/torque/server_priv/nodes",
         creates  => "/var/spool/torque/server_priv/nodes",
         path     => "/bin:/usr/bin:/usr/sbin:/sbin",
         cwd      => "/root",
         timeout  => 0,
         notify   => [
            Service["torque-server"],
            Service["torque-scheduler"],
         ],
      }
      package { torque-server:
         ensure  => latest,
         require => Exec[server_priv_nodes],
      }
      package { torque-scheduler:
         ensure  => latest,
         require => Exec[server_priv_nodes],
      }
      service { "torque-server":
         enable  => true,
         ensure  => running,
         require => File["/etc/torque/server_name"],
      }
      service { "torque-scheduler":
         enable  => true,
         ensure  => running,
         require => File["/etc/torque/server_name"],
      }
      file { "/usr/local/bin/config-queue":
         ensure  => file,
         owner   => root,
         group   => root,
         mode    => 700,
         source  => "puppet:///modules/torque/config-queue",
      }
      exec { "config-queue":
         command     => "/usr/local/bin/config-queue",
         creates     => "/var/spool/torque/server_priv/queues/batch",
         path        => "/bin:/usr/bin:/usr/sbin:/sbin",
         cwd         => "/home",
         timeout     => 0,
         require     => [
            File["/usr/local/bin/config-queue"],
            Package["torque-server"],
         ],
      }
   }
   else {
      package { torque-mom: 
         ensure  => latest,
         notify   => Service["torque-mom"],
      }
      service { "torque-mom":
         enable  => true,
         ensure  => running,
         require => [
            File["/etc/torque/server_name"],
            File["/var/spool/torque/pbs_environment"],
         ],
      }      
   }
   file { "/etc/torque/server_name":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 644,
      content => "master\n",
      require => Package[torque-common],
   }
   file { "/var/spool/torque/pbs_environment":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 644,
      content => "PATH=/bin:/usr/bin\n",
      require => Package[torque-common],
   }
}

