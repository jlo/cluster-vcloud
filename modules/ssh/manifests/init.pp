class ssh {
	package { openssh-server: 
		ensure  => latest,
	}
   file { "/root/.bashrc":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 600,
      source  => "puppet:///modules/ssh/bashrc.root",
   }
   file { "/root/.ssh":
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => 700,
   }
   file { "/root/.ssh/config":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 600,
      source  => "puppet:///modules/ssh/config",
      require => File["/root/.ssh"],
   }
   file { "/etc/skel/.ssh":
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => 700,
   }
   file { "/etc/skel/.ssh/config":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 600,
      source  => "puppet:///modules/ssh/config",
      require => File["/etc/skel/.ssh"],
   }
   file { "/etc/skel/.bashrc":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => 600,
      source  => "puppet:///modules/ssh/bashrc.user",
   }
}
