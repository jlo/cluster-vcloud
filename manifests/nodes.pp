stage { 'first': before => Stage['main'] }
stage { 'last': require => Stage['main'] }

node all {
   class { apt: stage => first; }
   include emacs
   include gcc
   include groups
   include htop
   include iptables
   include make
   include nfs
   include nmap
   class { root: stage => first; }
   include ssh
##   include torque
   include openmpi
   include gfortran
   include slurm
   include tree
   include users
   include vim
   include zip
}

node master inherits all {
}

node allnodes inherits all {
}

node default inherits allnodes {
}
