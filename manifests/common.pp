File { ignore => ['.svn', '.git', 'CVS' ] }

define chown($user,$group,$path) {
   exec { "chown -R ${user}:${group} ${path}":
      path    => '/bin:/sbin:/usr/bin:/usr/sbin',
   }
}

define chmod($droits,$path) {
   exec { "chmod -R ${droits} ${path}":
      path    => '/bin:/sbin:/usr/bin:/usr/sbin',
   }
}

